package main

import (
	"bytes"
	"crypto/sha512"
	"encoding/json"
	"encoding/hex"
	"fmt"
	"net/http"
	"strconv"
	"time"
	"net/url"
)

var (
	BOT_TOKEN = "TOKEN"
	CHAT_ID   = "ID"
)

// Blockchain is an array of block
type Blockchain struct {
	Timestamp int64    `json:"timestamp"`
	Blocks    []*Block `json:"blocks"`
}

// Block to be inserted into the
// blockchain
type Block struct {
	Hash      []byte `json:"hash,omitempty"`
	Previous  []byte `json:"previous,omitempty"`
	Timestamp []byte `json:"timestamp"`
	Payload   []byte `json:"payload"`
}

// Information is a the representation
// of an information
type Information struct {
	Title     string   `json:"title"`
	Timestamp int64    `json:"timestamp"`
	Content   string   `json:"content"`
	Keywords  []string `json:"keywords,omitempty"`
}

// NewInformation will return a new information
func NewInformation(title, content string, keywords ...string) *Information {
	return &Information{
		Title:     title,
		Timestamp: time.Now().Unix(),
		Content:   content,
		Keywords:  keywords,
	}
}

// NewBlock will create a new block to insert into
// the blockchain
func NewBlock(payload []byte) *Block {
	ts := []byte(strconv.FormatInt(time.Now().Unix(), 10))
	return &Block{
		Timestamp: ts,
		Payload:   payload,
	}
}

func (b *Block) setHash() {
	p := [][]byte{
		b.Timestamp,
		b.Previous,
		b.Payload,
	}
	h := sha512.Sum512(bytes.Join(p, []byte{}))
	b.Hash = h[:]
}

// NewBlockchain will init the blockchain
// with the genesis block
func NewBlockchain() *Blockchain {
	b := NewBlock([]byte("init block"))
	// there is no previous block
	b.Previous = []byte("0")
	b.setHash()

	c := &Blockchain{
		Timestamp: time.Now().Unix(),
		Blocks:    []*Block{b},
	}

	return c
}

// AddBlock will add a new block at the end of the
// blockchain
func (bc *Blockchain) AddBlock(b *Block) {
	// we fetch the the last hash in the chain
	prev := bc.Blocks[len(bc.Blocks)-1].Hash
	b.Previous = prev
	b.setHash()
	bc.Blocks = append(bc.Blocks, b)
	// we need to notify telegram channel
	notifyTelegram(b)
}

func notifyTelegram(b *Block) {
	inf := new(Information)
	if err := json.Unmarshal(b.Payload, inf); err != nil {
		fmt.Printf("unable to unmarshal json: %v", err)
	}
	txt := url.QueryEscape(inf.Title + "\n\n" + inf.Content + "\n\nhash digest: " + hexdigest(b.Hash))
	url := fmt.Sprintf(
		"https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s",
		BOT_TOKEN,
		CHAT_ID,
		txt,
	)
	if _, err := http.Get(url); err != nil {
		fmt.Println("unable to send message: %v", err)
	}
}

func hexdigest(h []byte) string {
	dst := make([]byte, hex.EncodedLen(len(h)))
	hex.Encode(dst, h)
	return "0x" + string(dst[:32])
}

func main() {
	i := NewInformation(
		"this IS information",
		"No more fake news, no more buzz news with this new innovative system. More information at this link: https://gitlab.com/TortueMat/datagram.git",
		"blockchain", "techno", "first",
	)
	j, _ := json.Marshal(i)
	b := NewBlock(j)
	c := NewBlockchain()
	c.AddBlock(b)
}
